# -*- coding: utf-8 -*-
# Copyright (C) garin <garin54@gmail.com> 2011-2018
# See the included file COPYING for details.
require_relative "mok2md_element"
require "cgi"
require "pry"

module Rd
  class Rd2Md
    VERSION = File.readlines(File.join(File.dirname(__FILE__),"../VERSION"))[0].strip
    RELEASE = File.readlines(File.join(File.dirname(__FILE__),"../RELEASE"))[0].strip

    def initialize(src, options = {})
      @debug = true

      # options
      @language = options[:language]
      @index = options[:index]
      @metadata = options[:metadata]
      @quiet = options[:quiet]
      get_customized_element(options[:custom_element]) unless options[:custom_element].empty?
      @rd = Mok::BlockParser.new(options)
      @nodes = @rd.parse src
      @metadata = setup_metadata unless @rd.metadata.empty?
    end

    # エレメントのカスタム用ファイルを読み込む
    def get_customized_element(file)
      require File.expand_path(file)
    end

    def setup_metadata
      @rd.metadata
    end

    def to_md
      md = ""
      md += metadata unless metadata_subject_only?
      md += header_title
      md += body
      md
    end

    def body
      @nodes.map do |node| node.apply end.join
    end

    def index
      return "" if @rd.index[:head].nil?
      str = "<div id='rd-index'>"
      level_pre = 1
      @rd.index[:head].each_with_index do |h,i|
        next if h[:level] == 1 or h[:level] == 6

        if h[:level] == 5
          str += %[<div class="nonum"><a href="#rd-head#{h[:level]}-#{i+1}"><span class="space" />#{h[:title]}</a></div>\n]
        else
          str += index_terminate(h[:level], level_pre)
          str += "<li><a href='#rd-head#{h[:level]}-#{i+1}'>#{h[:index]}#{h[:title]}</a>\n"
          level_pre = h[:level]
        end
      end
      str += index_terminate(2, level_pre) + "</ul>"
      str += "</div>"
      str
    end

    def index_terminate(level, level_pre)
      str = ""
      case level <=> level_pre
      when 1
        (level - level_pre).times do
          str += "<ul>"
        end
      when -1
        (level_pre - level).times do
          str += "</ul></li>"
        end
      else
        str += "</li>"
      end
      str
    end

    def metadata
      str = "---\n"
      str += @metadata.map do |data|
        case data[0]
        when :subject
          data[0] = :title
        when :create
          data[0] = :date
        end

        %[#{data[0]}: #{data[1]}]
      end.join("\n")
      str += "\n---\n"
      str
    end

    def footnote
      return "" if @rd.inline_index[:footnote].nil?
      str = ""
      @rd.inline_index[:footnote].each_with_index do |f,i|
        str += "[^#{i+1}]: #{f[:content].map{|c| c.apply}}\n"
      end
      str
    end

    def header_title
      "# #{@rd.metadata[:subject]}\n"
    end

    def metadata_subject_only?
      @metadata.size == 1 && @metadata.key?(:subject)
    end
  end
end

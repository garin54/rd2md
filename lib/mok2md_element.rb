# -*- coding: utf-8 -*-
# Copyright (C) garin <garin54@gmail.com> 2011-2018
# See the included file COPYING for details.
require 'bundler/setup'
require 'mok-parser'

# パーサとして mok-parser を使う
module Mok
  class Element
    def newline_to_br(str)
      str.gsub("\n", "\n")
    end

    # @inline_parser.parse の配列を文字列に変換する
    def inline_parse_to_str(array)
      str = ""
      array.each do |obj|
        str += obj.apply
      end
      str
    end
  end

  # ----- Blocks
  class WhiteLine < Element
    def apply
      "\n"
    end
  end

  class PlainTextBlock < Element
    def apply
      @contents.map {|c| c.apply }
    end
  end

  class HeadLine < Element
    # @contents = [level, title, id, index]
    def apply
      return "" if @contents[0] == 1
      return %[**#{@contents[1]}**\n] if @contents[0] == 5  # + header title
      return %[**#{@contents[1]}**\n] if @contents[0] == 6  # ++ header title      
      str = ""
      str += %[\n#{"#" * @contents[0].to_i} ]
      str += %[#{@contents[1]}\n]
      str
    end
  end

  class Paragraph  < Element
    def apply
      "#{@contents.map{|c| c.apply}}\n"
    end
  end

  class Quote < Element
    def apply
      # CGI.escapeHTML
      str = "\n"
      str += @contents.map do |line|
        "> #{line.apply}\n"
      end.join
      str
    end
  end

  class ItemList < Element
    def apply
      str = ""
      indent = 0
      @contents.map do |item|
        if item == :INDENT
          indent += 1
        elsif item == :DEDENT
          indent -= 1
        else
          str += "#{' ' * (indent * 2)}* #{item.apply}\n"
        end
      end
      str
    end
  end

  class NumList < Element
    def apply
      str = ""
      indent = 0
      num = [1]
      @contents.map do |item|
        if item == :INDENT
          indent += 1
          num[indent] = 1
        elsif item == :DEDENT
          indent -= 1
        else
          str += "#{' ' * (indent * 2)}#{num[indent]}. #{item.apply}\n"
          num[indent] += 1
        end
      end
      str
    end
  end

  class Desc < Element
    # @contents = [title, lines]
    def apply
      str = %[#{@contents[0]}\n]
      str += contents[1].first.apply.split("\n").map do |line|
        %[: #{line}] unless line == " "
      end.join("\n")
      str + "\n"
    end
  end

  class Table < Element
    def apply
      str = %[<table class="table table-hover">]
      @contents.each do |line|
        str += "\n<tr>"
        line.each do |item|
          if item =~ /^\s*\*/
            str += "<th>#{item.sub(/^\s*\*/, "").sub(/\*\s*$/,"")}</th>"
          else
            str += "<td>#{item}</td>"
          end
        end
        str += "\n</tr>"
      end
      str += "\n</table>"
      str
    end
  end

  class Preformat < Element
    def apply
      %[```\n#{@contents.join}\n```\n]
    end
  end
  
  # --- Blocks end

  # --- Inlines
  class Label < Element
    # @contents = [label, title]
    def apply
      %[<a name="#{@contents[0]}" id="#{@contents[0]}">#{@contents[1]}</a>]
    end
  end

  class LabelLink < Element
    # @contents = [label, title]
    def apply
      %[<a href="##{@contents[0]}">#{@contents[1]}</a>]
    end
  end

  class Reference < Element
    def apply
      "[#{@contents[0]}](#{@contents[1]})"
    end
  end

  class Plain < Element
    def apply
      "#{newline_to_br(CGI.escapeHTML(@contents.join))}"
    end
  end

  class Emphasis       < Element
    def apply
      " **#{@contents.map{|c| c.apply}}** "
    end
  end
  class Italic       < Element
    def apply
      " _#{@contents.map{|c| c.apply}}_ "
    end
  end
  class Strike       < Element
    def apply
      " ~~#{@contents.map{|c| c.apply}}~~ "
    end
  end
  class Code       < Element
    def apply
      " ```#{@contents.map{|c| c.apply}}``` "
    end
  end
  class Kbd       < Element
    def apply
      "<kbd>#{@contents.map{|c| c.apply}}</kbd>"
    end
  end
  class Verb       < Element
    def apply
      "#{@contents}"
    end
  end

  class Manuedo       < Element
    def apply
      %{<span class="manuedo">[#{@contents.map{|c| c.apply}}]</span>}
    end
  end
  class Ruby       < Element
    def apply
      "<ruby><rb>#{@contents[0]}</rb><rp>(</rp><rt>#{@contents[1]}</rt><rp>)</rp></ruby>"
    end
  end
  class Variable       < Element
    def apply
      "#{@contents}"
    end
  end

  class Footnote       < Element
    def apply
      "[^#{@contents[1]}]"
    end
  end

  class Media       < Element
    def apply
      # @contents = [Name, Mime::MediaType, Mime::SubType]
      case @contents[1]
      when 'image'
        "![#{@contents[0]}](#{@contents[0]})"
      when 'video'
        %[<video src="#{@contents[0]}" controls></video>]
      when 'audio'
        %[<audio src="#{@contents[0]}" controls></audio>]
      else
        "[#{@contents[0]}](#{@contents[0].split("/").last})"
      end
    end
  end
end

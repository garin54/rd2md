# rd2md

Ruby の [RDoc](https://github.com/ruby/rdoc) 形式または [Mok](http://garin.jp/2013/12/12/mok.html) 形式で書かれたドキュメントを Markdown 形式に変換します。

## Usage
### require

* ruby-2.5.x

### Install

``` console
$ git clone https://gitlab.com/garin54/rd2md
$ cd rd2html
$ bundle
```

### usage

``` console
//  markdown transform to stdout
$ ./bin/rd2md hoge.rd
```

## 変換できる書式

### 対応済み
以下の書式には対応しています。

* タイトルヘッダ(h1)
* 章ヘッダ(h2-h4)
* 通し番号のない項目(+)、整形済みの見出し(++) → 強調(**)に変換
* 文字修飾(強調、斜体、否定)
* URL リンク
* コードブロック
* 記号付き箇条書き
* 番号付き箇条書き
* 注釈(footnote)
* HTML タグ
* mok メタデータ(yaml metadata に変換)


### HTML として出力するもの
Markdown に変換ではなく HTML に変換して出力します。

* Mok 形式のテーブル
  * → php markdown と Mok で仕様が違う
* 文字修飾の一部(ルビ、キーボード、動画、音声)
  * → Markdown が未対応
* ラベルリンク
  * → Markdown が未対応

### 未対応書式

Rdoc/Mok と Markdown の非互換のため一部変換できない書式があります。

* コメント(削除される; mok-parser の対応が必要)
* ラベル付きリスト
* 文字修飾の一部(文字修飾の無効化、タイプライター体)
* マルチメディアの一部
* mok の独自機能
  * 変数
  * 校正(真鵺道)
  * ドキュメント内プログラム(erb)
  * 部分テンプレート
  * 外部変数ファイル
  * 整形済みの見出し(++) → 強調に変換

## 免責
自分が必要だったので作成したプログラムです。at your own risk でおねがいします。

## Copyright
Copyright (c) 2011-2018 garin. See LICENSE.txt for
further details.

= サンプル用のRDocファイル

以下のコマンドで変換のテストします

  $ ./bin/rd2md test.md 

== inline
* ((*太字*))
* ((_斜体_))
* ((-否定-))
* コードブロック: (({sample = "str" if sample.nil? }))

=== URL
* [google](https://google.com)

== block
=== code

  class Hello
    puts :hello
  end
    
=== table

|aaa|bbb|
|ccc|ddd|

=== list
* aaa
  * AAA
  * BBB
* BBB
* CCC
  
---

1. 111
2. 222


=== definition
:用語名
  説明文1
  説明文2
  
=== footnote
ここの注釈があります (([注釈はこのように表示されます]))
2つ目の注釈 (([2つ目の注釈はここに表示されます]))
